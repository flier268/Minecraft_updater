﻿using System.Text.RegularExpressions;

namespace Minecraft_updater
{
    public struct Pack
    {
        public bool Delete { get; set; }
        public bool CheckMD5 { get; set; }
        public string MD5 { get; set; }
        public string URL { get; set; }
        public string Path { get; set; }
        public bool NameFuzzy { get; set; }
    }
    public static class Packs
    {
        static Regex r = new Regex("(.*?)\\|\\|(.*?)\\|\\|(.*)", RegexOptions.Singleline);
        public static Pack reslove(string s)
        {
            Match m;
            bool delete = false;
            bool CheckMD5 = true;
            bool NameFuzzy = false;
            int Index_Star = -1;
            if (s.StartsWith("#"))
            {
                delete = true;
                m = r.Match(s.Substring(1, s.Length - 1));
                if (m.Success)
                {
                    Index_Star = m.Groups[1].ToString().IndexOf('*');
                    if (Index_Star != -1)
                        NameFuzzy = true;
                }
            }
            else if (s.StartsWith(":"))
            {
                CheckMD5 = false;
                m = r.Match(s.Substring(1, s.Length - 1));
            }
            else
                m = r.Match(s);
            if (m.Success)
            {
                return new Pack { Path = NameFuzzy ? m.Groups[1].ToString().Substring(0, Index_Star) : m.Groups[1].ToString(), MD5 = ((m.Groups[2] == null) ? "" : m.Groups[2].ToString()), URL = ((m.Groups[3] == null) ? "" : m.Groups[3].ToString()), Delete = delete, NameFuzzy = NameFuzzy, CheckMD5 = CheckMD5 };
            }
            else
                return new Pack { };
        }
    }
}
